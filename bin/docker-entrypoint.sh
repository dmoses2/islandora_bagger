#!/usr/bin/env bash
set -e

if [[ -n "${DEBUG}" ]]; then
  set -x
fi

# Make sure that files dir have proper permissions.
mkdir -p ${FILES_DIR}/private
# Ensure the files dir is owned by nginx user
chown -R ${NGINX_USER}:${NGINX_USER_GROUP} ${FILES_DIR}

exec "$@"
